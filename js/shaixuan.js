$.fn.smartFloat = function () {
    var position = function (element) {
        var top = element.position().top,
            pos = element.css("position");
        $(window).scroll(function () {
            var scrolls = $(this).scrollTop();
            if (scrolls > top) {
                if (window.XMLHttpRequest) {
                    $(".panel-container").css({
                        position: 'fixed'
                    });
                    element.css({
                        position: "fixed",
                        top: 0
                    });
                } else {
                    element.css({
                        top: scrolls
                    });
                }
            } else {
                $(".panel-container").css({
                    position: 'static'
                });
                element.css({
                    position: pos,
                    top: top
                });
            }
        });
    };
    return $(this).each(function () {
        position($(this));
    });
};
// 调用筛选条件的方法
$("#tabNav").smartFloat();
// tab切换筛选条件
$(".sxBtn .tab-item").click(function () {
    var idx = $(this).index();
    // 遮罩层
    $('.maskWrap').addClass('panel-mask');
    // 禁止滚动
    $('body').addClass('lock-scroll');
    // 只要点击筛选就将筛选条件置顶
    $("#tabNav").css({
        position: 'fixed',
        top: '0'
    });
    $(".panel-container").css({
        position: 'fixed'
    });
    $(this).addClass('active-show').siblings().removeClass('active-show');
    $(".sxContent").eq(idx).removeClass('hide').siblings().addClass('hide');
});
// 关闭遮罩层
$('.maskWrap').on('click', function () {
    // 移除遮罩层
    $('.maskWrap').removeClass('panel-mask');
    // 移除禁止滚动
    $('body').removeClass('lock-scroll');
    $("#tabNav").css({
        position: 'static'
    });
    $('.sxBtn .tab-item').removeClass('active-show');
    $(".sxContent").addClass('hide');
});
// 选择满五年切换样式
$('.conch-ui-tab-item').click(function () {
    if ($(this).hasClass('conch-ui-active')) {
        $(this).removeClass('conch-ui-active');
    } else {
        $(this).addClass('conch-ui-active');
    }
});
// 区域切换
$(".conch-ui-col1 .conch-ui-item").click(function () {
    $(this).addClass('conch-ui-active').siblings().removeClass('conch-ui-active');
});
$(".conch-ui-col2 .conch-ui-item").click(function () {
    $(this).addClass('conch-ui-active').siblings().removeClass('conch-ui-active');
});
$(".conch-ui-col3 .conch-ui-item").click(function () {
    $(this).addClass('conch-ui-active').siblings().removeClass('conch-ui-active');
    $(".maskWrap").click();
});

// 价格、房型、更多选中切换
$('.conch-ui-check-items .conch-ui-check-item').click(function () {
    if ($(this).hasClass('conch-ui-active')) {
        $(this).removeClass('conch-ui-active');
    } else {
        $(this).addClass('conch-ui-active');
    }
});
// 排序选中关闭弹窗
$('.sxContent1 .conch-ui-check-item').click(function () {
    $('.maskWrap').click();
    $('.sxBtn .tab-item:last span').html($(this).text());
})
// 价格查询按钮
$('.priceBtn').click(function () {
    $(".maskWrap").click();
});
// 房型查询按钮
$('.fxBtn').click(function () {
    $(".maskWrap").click();
});
// 更多查询按钮
$('.moreBtn').click(function () {
    $(".maskWrap").click();
});
// 价格重置按钮
$('.jgReset').click(function () {
    $(this).parent().siblings().children().children().children().removeClass('conch-ui-active');
});