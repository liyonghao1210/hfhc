(function winInitFontSize(){
    window.onresize = initFontSize;
    function initFontSize(){
        var winWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        //rem的基数设置大一点，可以减少误差
        document.getElementsByTagName("html")[0].style.fontSize = ((winWidth > 750 ? 750 : winWidth)/750) * 100 + 'px';
    }
    initFontSize();
})();